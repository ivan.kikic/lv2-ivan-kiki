﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class DiceRoller :ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger logger;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }

        public void InsertDie(Die die)
        {
            dice.Add(die);
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
                this.resultForEachRoll);
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }

        public void SetLogger(ILogger logger)
        {
            this.logger = logger;
        }

        //public void LogRollingResults()
        //{
        //    foreach (int result in this.resultForEachRoll)
        //    {
        //        logger.Log(result.ToString());
        //    }
        //}

        public virtual void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }

        public string GetStringRepresentation()
        {
            StringBuilder builder = new StringBuilder();
            foreach (int r in resultForEachRoll)
            {
                builder.Append(resultForEachRoll.ToString()).Append("\n");
            }
            return builder.ToString();
        }
    }
}
