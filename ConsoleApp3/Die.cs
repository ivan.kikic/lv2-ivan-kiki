﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;

    //    public Die(int numberOfSides)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = new Random();
    //    }

    //    public Die(int numberOfSides, Random generator)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = generator;
    //    }

    //    public int Roll()
    //    {
    //        int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
    //        return rolledNumber;
    //    }
    //}

    class Die
    {
        private int numberOfSides;

        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
        }

        public int Roll()
        {
            int rolledNumber = RandomGenerator.GetInstance().NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
    }
}
