﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller roller = new DiceRoller();

            int n = 20; int i;
           for(i = 0; i < n; i++)
            {
                roller.InsertDie(new Die(6));
            }
            roller.RollAllDice();
            IList<int> results1 = roller.GetRollingResults();
            foreach (int r in results1)
            {
                Console.WriteLine(r);
            }

            Random generator = new Random();
            for (i = 0; i < n; i++)
            {
                roller.InsertDie(new Die(6, generator));
            }
            roller.RollAllDice();
            IList<int> results2 = roller.GetRollingResults();
            foreach (int r in results2)
            {
                Console.WriteLine(r);
            }
        }
    }
}
