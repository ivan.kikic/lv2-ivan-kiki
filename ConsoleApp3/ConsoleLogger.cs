﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class ConsoleLogger : ILogger
    {
        public ConsoleLogger()
        {

        }
        public void Log(ILogable message)
        {
            //Console.WriteLine(message);
            Console.WriteLine(message.GetStringRepresentation());
        }
    }
}
