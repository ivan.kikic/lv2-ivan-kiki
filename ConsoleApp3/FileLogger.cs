﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class FileLogger : ILogger
    {
        private string filePath;
        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }

        public void Log(ILogable message)
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath))
            {
                fileWriter.WriteLine(message.GetStringRepresentation());
            }
        }
    }
}
